# Ce projet est sous licence MIT

Voir le fichier Licence.md

# Auteurs

Jasmin Bergeron-Joubert

# Serveur discord du projet

https://discord.gg/BWb9mjgq

# Description générale

Ce projet Android a pour but de motiver les enfants à faire leurs tâches 
ménagères en ludifiant leur expérience, c’est-à-dire de donner un aspect « jeu »
à ces tâches. Le parent commence par entrer les tâches à effectuées
par les enfants et leur valeur en points, ainsi que la liste des récompenses 
possible d'obtenir et ensuite les enfants tentent de vaincre un « ennemi » 
commun en complétant leurs tâches. Cet « ennemi » subit des dégâts en
choisissant la tâche qui a été faite, selon la valeur de ses points.
« L’ennemi » est vaincu une fois que toutes les tâches ont été accomplies et
approuvées par le juge (généralement, le parent) et un butin apparait sous la 
forme d’un choix parmi les récompenses sélectionnées.

# Standard de programmation à respecter

•	La taille maximale d'une ligne est de 120 caractères;
•	Chaque indentation est de 4 espaces;
•	Toutes les structures de contrôles (routines, if, for, while, etc.) doivent 
    être délimitées par des accolades ('{' et '}');
•	Tous les mots-clés et opérateurs (if, for, +, =) doivent être entourés de 
    caractère d'espacement;
•	Les indicateurs de variables, arguments, attributs, routines:
    o	Débutent toujours par une lettre minuscule;
    o	Doivent être écrit en minuscule sauf la première lettre de chaque mot 
    qui doit être en majuscule;
•	Les indicateurs de classes :
    o	Débutent toujours par une lettre majuscule;
    o	Doivent être écrit en minuscule sauf la première lettre de chaque mot 
        qui doit être en majuscule;
•	Les indicateurs de constantes:
    o	Doivent être écrit entièrement en majuscule;
    o	Chaque mot doit être séparé par un tiret bas (“_”);
    o	Doivent être défini à l’aide du mot clé « final »;
•	Tous les fichiers .java doivent avoir une documentation de type Javadoc 
    indiquant:
    o	Le ou les auteurs (indiqués par l’indicateur “@author”);
    o	La version du fichier (indiqué par l’indicateur “@version”):
        	Utilisez “@version %I%, %G%”,
    o	Une description du fichier (généralement de la classe);
•	Les routines:
    o	La signature (définition) possède une indentation par rapport à la
        définition de la classe;
    o	L’accolade ouvrante de la routine est placée sur la même ligne que la 
        signature;
    o	L’accolade fermante est placée seule sur sa ligne à la même indentation 
        que la signature de la routine;
    o	Le nombre de lignes maximal d'une routine est de 45 lignes en incluant 
        la documentation;
    o	Une routine devant retourner une valeur doit avoir un seul « return », 
        placé à la dernière ligne de la routine (la ligne avant l’accolade 
        fermante de la routine);
    o	Une routine ne retournant aucune valeur de retour ne doit avoir aucun 
        « return »;
    o	Une documentation de type Javadoc doit accompagner toutes les routines.
        Cette documentation doit contenir:
        	Une description brève sur la première ligne du contenu de la 
            documentation;
        	Optionnellement une description longue;
        	Une description de tous les arguments (indiqué par l'indicateur 
            “@param”);
        	Une description de la valeur de retour s'il y en a une (indiqué par 
            l'indicateur “@return”);
        	Optionnellement, des notes que le programmeur lecteur doit prendre 
            en compte (indiqué par l'indicateur “@note”);
        	Idéalement, une fonction retournant une valeur ne devrait pas faire 
            d’effet de bord;
            	Si cette règle n’est pas respectée pour une raison précise, une 
            indication de l’effet de bord attendu devra être précisée dans la 
            documentation;
•	Les autres structures de contrôle:
    o	L’accolade ouvrante est placée à la fin de la ligne contenant le mot clé
        de la structure de contrôle et est précédée par un espace;
    o	Sauf pour le “else”, l'accolade fermante est placée seul sur une ligne 
        à la même colonne que le mot clé de structure de contrôle;
    o	Pour le “else”, l'accolade fermante du “if” est placée avant le mot clé 
        “else”, sur la même ligne et séparé par un espace;
    o	Sauf pour la structure de contrôle « switch », aucun « break » ou
        « continue » ne doit être utilisé dans le code;
    o	Pour le « switch », tous les « case » doivent se terminer par un
        « break »;
•	Les classes :
    o	La définition de la classe est placée à la première colonne du ficher;
    o	Autre que les constantes, aucune variable globale ne peut être 
        accessible dans une classe;
    o	Aucun attribut ne devra être pré-initialisé dans la déclaration. Les 
        attributs devront être initialisés dans le constructeur;

# Instructions d'installation

- Création d'un compte Gitlab.
- Suivre les instructions de Gitlab.
- Se connecter sur votre compte Gitlab.
- Faire un git clone avec la commande:
  git clone https://gitlab.com/jasmin.bergeron-joubert/home-tasks-titans.git
- Mettre votre telephone android en mode developpeur
    1. Paramètres
    2. À propos du téléphone
    3. Informations sur le logiciel
    4. Tapez 7 fois sur Numéro de version
    5. Entrez votre nip
    6. Revenez en arrière deux fois
    7. Tout en bas, Option de développement est débloqué
    8. Dans Options de développement, activez débogage usb
- Branchez votre telephone à l'ordinateur
- Démarrer le projet dans votre ide avec votre telephone sélectionné.

# Les sons utilisés

- Effet sonore "ahh" par jrssandoval de Pixabay.com
- Effet sonore "Swinging staff whoosh (strong) 08" de Pixabay
- Effet sonore "menu selection" par morganpurkis de Pixabay

# Les images utilisé

Pieuvre par TheGeorgeBond de Pixabay

toutes les autres ont aussi été trouvé sur Pixabay