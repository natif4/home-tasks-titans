package com.example.home_tasks_titans;


import android.graphics.Color;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.home_tasks_titans.Holders.Holder;
import com.example.home_tasks_titans.Holders.HolderRecompense;
import com.example.home_tasks_titans.Holders.HolderTache;
import com.example.home_tasks_titans.Ressources.Ressource;
import com.example.home_tasks_titans.Ressources.RessourceRecompense;
import com.example.home_tasks_titans.Ressources.RessourceTache;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jasmin Bergeron-Joubert
 * @version %I%,%G%
 */

/**
 * Classe qui rempli les vues holder avec les données des ressources reçues
 */
public class Adaptateur extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /**
     * La liste des ressources de l'adaptateur
     */
    public ArrayList<Ressource> ressources;

    /**
     * L'identification des types de ressources
     */
    private final int TYPE_TACHE = 0, TYPE_RECOMPENCE = 1;

    /**
     * Si les vues modifier et supprimer des holder sont visibles
     */
    private boolean peutAlterer;

    /**
     * Si le holder sert pour le conteneur de setupPartie
     */
    private boolean pourSetup;

    /**
     * La liste des tâches sélectionnées pour la partie dans le conteneur de setup
     */
    private ArrayList<RessourceTache> tachesSelectionnees;

    /**
     * La liste des récompenses sélectionnées pour la partie dans le conteneur de setup
     */
    private ArrayList<RessourceRecompense> recompensesSelectionnees;

    /**
     * La position de l'item sélectionné dans la partie pour faire peur au monstre.
     *
     * J'ai mis une liste pour pouvoir la passer dans les constructeurs, un int ne fonctionne pas.
     */
    private ArrayList<Integer> positionSelectionnee;


    /**
     * Constructeur pour le setup de partie
     * @param ressources La liste des ressources à mettre dans les holders.
     * @param peutAlterer Indique si les vues modifier et supprimer des holders
     *                   sont visibles.
     * @param pourSetup Indique si les holder servent pour le setup.
     * @param tachesSelectionnees La liste des tâches sélectionnées.
     * @param recompensesSelectionnees La liste des récompenses sélectionnées.
     *
     */
    public Adaptateur(List<? extends Ressource> ressources, boolean peutAlterer, boolean pourSetup,
                      ArrayList<RessourceTache> tachesSelectionnees,
                      ArrayList<RessourceRecompense> recompensesSelectionnees) {
        this.ressources = (ArrayList<Ressource>) ressources;
        this.peutAlterer = peutAlterer;
        this.pourSetup = pourSetup;
        this.tachesSelectionnees = tachesSelectionnees;
        this.recompensesSelectionnees = recompensesSelectionnees;
    }

    /**
     * Constructeur de base
     * @param ressources La liste des ressources à mettre dans les holders.
     * @param peutAlterer Indique si les vues modifier et supprimer des holders
     *                   sont visibles.
     */
    public Adaptateur(List<? extends Ressource> ressources, boolean peutAlterer) {
        this.ressources = (ArrayList<Ressource>) ressources;
        this.peutAlterer = peutAlterer;
    }

    /**
     * Constructeur pour le cas d'une partie
     * @param ressources La liste des ressources à mettre dans les holders.
     * @param peutAlterer Indique si les vues modifier et supprimer des holders
     *                   sont visibles.
     * @param pourSetup Indique si les holder servent pour le setup.
     * @param positionSelectionnee Indique à la parite quelle est la position du holder qui à été sélectionné.
     */
    public Adaptateur(List<? extends Ressource> ressources, boolean peutAlterer, boolean pourSetup,
                      ArrayList<Integer> positionSelectionnee) {
        this.ressources = (ArrayList<Ressource>) ressources;
        this.peutAlterer = peutAlterer;
        this.pourSetup = pourSetup;
        this.positionSelectionnee = positionSelectionnee;
    }

    /**
     * Action à effectuer à la création de l'adaptateur
     * @param parent La vue à laquelle la nouvelle vue sera ajoutée après être
     *              liée à la position de l'adaptateur.
     * @param viewType Le type de vue de la nouvelle vue.
     *
     * @return La nouvelle vue (holder).
     */
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Holder holder = null;
        View layoutFragment;
        if (viewType == TYPE_TACHE) {
            layoutFragment = LayoutInflater.from(parent.getContext())
                    .inflate((R.layout.holder_tache), parent, false);
            holder = new HolderTache(layoutFragment, peutAlterer, this);
        } else if (viewType == TYPE_RECOMPENCE) {
            layoutFragment = LayoutInflater.from(parent.getContext())
                    .inflate((R.layout.holder_recompense), parent, false);
            holder = new HolderRecompense(layoutFragment, peutAlterer, this);
        }

        return holder;
    }

    /**
     * Retourne le type de la vue
     *
     * @param position la position de la vue dans la liste
     * @return le type de la vue
     */
    @Override
    public int getItemViewType(int position) {
        int typeVue = 0;
        if (ressources.get(position) instanceof RessourceTache) {
            typeVue = TYPE_TACHE;
        } else if (ressources.get(position) instanceof RessourceRecompense) {
            typeVue = TYPE_RECOMPENCE;
        }
        return typeVue;
    }

    /**
     * Assigne les données ressources aux vues des holders
     * @param holder Le holder qui doit être mis à jour avec les données
     *               ressources de sa position.
     * @param position La position dans la liste des ressources de l'adaptateur.
     */
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HolderTache) {
            RessourceTache ressource = (RessourceTache) ressources.get(position);
            ((HolderTache) holder).getTextViewDescription().setText(ressource.getDescription());
            ((HolderTache) holder).getTextViewValeur().setText(ressource.getValeur());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MediaPlayer selection = MediaPlayer.create(view.getContext(), R.raw.menu_selection);
                    if (!peutAlterer && pourSetup) {
                        if (!tachesSelectionnees.contains(ressource)) {
                            selection.start();
                            tachesSelectionnees.add(ressource);
                            holder.itemView.setBackgroundColor(Color.BLACK);
                        } else {
                            selection.start();
                            tachesSelectionnees.remove(ressource);
                            holder.itemView.setBackgroundColor(Color.DKGRAY);
                        }
                    } else if (!peutAlterer && !pourSetup) {
                        if (positionSelectionnee.size() == 1) {
                            if (holder.getAdapterPosition() == positionSelectionnee.get(0)) {
                                selection.start();
                                positionSelectionnee.clear();
                                holder.itemView.setBackgroundColor(Color.BLACK);
                            }
                        } else {
                            selection.start();
                            positionSelectionnee.add(holder.getAdapterPosition());
                            holder.itemView.setBackgroundColor(Color.DKGRAY);
                        }
                    }
                }
            });
        } else if (holder instanceof HolderRecompense) {
            RessourceRecompense ressource = (RessourceRecompense) ressources.get(position);
            ((HolderRecompense) holder).getTextViewDescription().setText(ressource.getDescription());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MediaPlayer selection = MediaPlayer.create(view.getContext(), R.raw.menu_selection);
                    if (!peutAlterer && pourSetup) {
                        if (!recompensesSelectionnees.contains(ressource)) {
                            selection.start();
                            recompensesSelectionnees.add(ressource);
                            holder.itemView.setBackgroundColor(Color.BLACK);
                        } else {
                            selection.start();
                            recompensesSelectionnees.remove(ressource);
                            holder.itemView.setBackgroundColor(Color.DKGRAY);
                        }
                    }
                }
            });
        }
    }

    /**
     * Permet de savoir combien de ressources sont dans la liste de l'adaptateur.
     *
     * @return Le nombre d'items dans la liste de ressources
     */
    @Override
    public int getItemCount() {
        int compte;
        if (ressources == null) {
            compte = 0;
        } else {
            compte = ressources.size();
        }
        return compte;
    }
}
