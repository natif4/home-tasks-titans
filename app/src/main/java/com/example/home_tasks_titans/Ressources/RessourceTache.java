package com.example.home_tasks_titans.Ressources;

/**
 * @author Jasmin Bergeron-Joubert
 * @version %I%,%G%
 */

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

/**
 * Classe qui contient les éléments pour une tâche.
 */
public class RessourceTache extends Ressource implements Parcelable {

    /**
     * La valeur en point de la tâche.
     */
    private String valeur;

    /**
     * Constructeur
     *
     * @param description La tâche à faire.
     * @param valeur Son pointage.
     */
    public RessourceTache(String description, String valeur){
        super(description);
        this.valeur = valeur;
    }

    /**
     * Constructeur pour l'utilisation du parcel.
     *
     * @param in le parcel à utiliser.
     */
    protected RessourceTache(Parcel in) {
        valeur = in.readString();
        description = in.readString();
    }

    /**
     * Interface pour la création d'un RessourceRecompense à partir d'un Parcel.
     */
    public static final Creator<RessourceTache> CREATOR = new Creator<RessourceTache>() {

        /**
         * Crée une instance de l'objet à partir du Parcel spécifié.
         *
         * @param in Le Parcel spécifié.
         * @return L'objet créé à partir du Parcel.
         */
        @Override
        public RessourceTache createFromParcel(Parcel in) {
            return new RessourceTache(in);
        }

        /**
         * Crée un tableau de l'objet.
         *
         * @param size La taille du tableau à créer.
         * @return Un tableau de l'objet de la taille spécifiée.
         */
        @Override
        public RessourceTache[] newArray(int size) {
            return new RessourceTache[size];
        }
    };

    /**
     * Retourne la valeur de la tâche.
     * @return la valeur de la tâche.
     */
    public String getValeur(){
        return valeur;
    }

    /**
     * Défini la valeur de la tâche.
     * @param valeur La valeur en pointage de la tâche.
     */
    public void setValeur(String valeur){
        this.valeur = valeur;
    }

    /**
     * Retourne les indicateurs de contenu de l'objet.
     *
     * @return Les indicateurs de contenu de l'objet. Cette méthode retourne 0 ou l'une des constantes
     *         suivantes : CONTENTS_FILE_DESCRIPTOR, CONTENTS_TEXT ou CONTENTS_UNDEFINED.
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Écrit les valeurs des champs de l'objet dans le Parcel spécifié.
     *
     * @param parcel Le Parcel dans lequel les valeurs doivent être écrites.
     * @param i indication supplémentaire de comment le parcel devrait être écrit.
     */
    @Override
    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(valeur);
        parcel.writeString(description);
    }
}
