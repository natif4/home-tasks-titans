package com.example.home_tasks_titans.Ressources;

/**
 * @author Jasmin Bergeron-Joubert
 * @version %I%,%G%
 */

/**
 * Classe abstraite qui contient les données pour l'affichage dans les holders.
 */
public abstract class Ressource {

    /**
     * La description de la ressource.
     */
     protected String description;

    /**
     * Constructeur
     * @param description La description de la ressource.
     */
    public Ressource(String description){
        this.description = description;
    }

    protected Ressource() {
    }

    /**
     * Retourne la description de la ressource.
     * @return la description de la ressource.
     */
    public String getDescription(){
        return description;
    }

    /**
     * Défini la description de la ressource.
     * @param description La description de la ressource.
     */
    public void setdescription(String description){
        this.description = description;
    }
}
