package com.example.home_tasks_titans.Ressources;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

/**
 * @author Jasmin Bergeron-Joubert
 * @version %I%,%G%
 */

/**
 * Classe qui représente les informations à mettres dans les vues des récompenses.
 */
public class RessourceRecompense extends Ressource implements Parcelable {

    /**
     * Constructeur
     *
     * @param description La description de la récompense.
     */
    public RessourceRecompense(String description) {
        super(description);
    }

    /**
     * Constructeur pour l'utilisation du parcel.
     *
     * @param in le parcel à utiliser.
     */
    protected RessourceRecompense(Parcel in) {
        description = in.readString();
    }

    /**
     * Interface pour la création d'un RessourceRecompense à partir d'un Parcel.
     */
    public static final Creator<RessourceRecompense> CREATOR = new Creator<RessourceRecompense>() {

        /**
         * Crée une instance de l'objet à partir du Parcel spécifié.
         *
         * @param in Le Parcel spécifié.
         * @return L'objet créé à partir du Parcel.
         */
        @Override
        public RessourceRecompense createFromParcel(Parcel in) {
            return new RessourceRecompense(in);
        }

        /**
         * Crée un tableau de l'objet.
         *
         * @param size La taille du tableau à créer.
         * @return Un tableau de l'objet de la taille spécifiée.
         */
        @Override
        public RessourceRecompense[] newArray(int size) {
            return new RessourceRecompense[size];
        }
    };

    /**
     * Retourne les indicateurs de contenu de l'objet.
     *
     * @return Les indicateurs de contenu de l'objet. Cette méthode retourne 0 ou l'une des constantes
     *         suivantes : CONTENTS_FILE_DESCRIPTOR, CONTENTS_TEXT ou CONTENTS_UNDEFINED.
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Écrit les valeurs des champs de l'objet dans le Parcel spécifié.
     *
     * @param parcel Le Parcel dans lequel les valeurs doivent être écrites.
     * @param i indication supplémentaire de comment le parcel devrait être écrit.
     */
    @Override
    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(description);
    }
}
