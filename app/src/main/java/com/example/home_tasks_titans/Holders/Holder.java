package com.example.home_tasks_titans.Holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.home_tasks_titans.Adaptateur;

/**
 * @author Jasmin Bergeron-Joubert
 * @version %I%,%G%
 */


/**
 * Gère les informations communes à tous les types de holder
 */
public abstract class Holder extends RecyclerView.ViewHolder {

    /**
     * La vue de la description du holder.
     */
    protected TextView description;

    /**
     * Les vues qui permettent de modifier et de supprimer le holder.
     */
    protected ImageView modifier, supprimer;

    /**
     * L'adaptateur qui gère le holder.
     */
    protected Adaptateur adaptateur;

    /**
     * Constructeur
     * @param vue la vue du créateur du holder
     */
    public Holder(@NonNull View vue) {
        super(vue);
    }


}
