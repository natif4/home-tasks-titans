package com.example.home_tasks_titans.Holders;

import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.home_tasks_titans.Adaptateur;
import com.example.home_tasks_titans.GestionFichier;
import com.example.home_tasks_titans.R;

/**
 * @author Jasmin Bergeron-Joubert
 * @version %I%,%G%
 */

/**
 * Permet d'afficher les éléments des ressourceRecompense
 */
public class HolderRecompense extends Holder {

    /**
     * La vue du créateur du holder.
     */
    private View vue;

    /**
     * Constructeur
     * @param vue La vue du créateur du holder
     * @param peutAlterer La vue du créateur du holder
     * @param adaptateur L'adaptateur qui gère le holder.
     */
    public HolderRecompense(@NonNull View vue, boolean peutAlterer, Adaptateur adaptateur) {
        super(vue);
        this.vue = vue;
        this.adaptateur = adaptateur;
        description = vue.findViewById(R.id.descriptionRecompense);
        modifier = vue.findViewById(R.id.modifierRecompense);
        supprimer = vue.findViewById(R.id.supprimerRecompense);
        if (!peutAlterer) {
            modifier.setVisibility(View.GONE);
            supprimer.setVisibility(View.GONE);
        }
        else {
            modifierRecompense();
            supprimer();
        }
    }

    /**
     * Action à faire à l'appuie du bouton modifier
     */
    private void modifierRecompense() {
        modifier.setOnClickListener(View -> {
            popDialogueModification();
        });
    }

    /**
     * Action à faire à l'appuie du bouton supprimer
     */
    private void supprimer() {
        supprimer.setOnClickListener(View -> {
            adaptateur.ressources.remove(getBindingAdapterPosition());
            adaptateur.notifyItemRemoved(getBindingAdapterPosition());
            GestionFichier.mettreAJourFichier(vue, adaptateur.ressources,
                    "Recompenses");
        });
    }

    /**
     * Ouvre une boîte de dialogue pour modifier le texte de la description.
     */
    private void popDialogueModification() {
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(vue.getContext());
        builder.setTitle("Nouveaux nom de la récompense?");

        LayoutInflater inflater = LayoutInflater.from(vue.getContext());
        View layout = inflater.inflate(R.layout.layout_dialogue_recompense, null);
        builder.setView(layout);
        final EditText input = layout.findViewById(R.id.champ_nom_recompense);
        input.setText(description.getText());

        builder.setPositiveButton("Modifier",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        description.setText(input.getText().toString());
                        adaptateur.ressources.get(getBindingAdapterPosition())
                                .setdescription(input.getText().toString());
                        GestionFichier.mettreAJourFichier(vue, adaptateur.ressources, "Recompenses");
                    }
                });
        builder.setNegativeButton("Annuler",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.show();
    }
    /**
     * Retourne La vue de la description.
     * @return
     */
    public TextView getTextViewDescription() {
        return description;
    }
}
