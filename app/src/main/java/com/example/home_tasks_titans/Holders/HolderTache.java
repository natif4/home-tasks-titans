package com.example.home_tasks_titans.Holders;

import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.home_tasks_titans.Adaptateur;
import com.example.home_tasks_titans.GestionFichier;
import com.example.home_tasks_titans.R;
import com.example.home_tasks_titans.Ressources.RessourceTache;

/**
 * @author Jasmin Bergeron-Joubert
 * @version %I%,%G%
 */

/**
 * Permet d'afficher les élément des ressourceTache
 */
public class HolderTache extends Holder {

    /**
     * La vue de la valeur de la tâche.
     */
    private final TextView valeur;

    /**
     * La vue du créateur du holder.
     */
    private View vue;

    /**
     * Constructeur
     *
     * @param vue La vue du créateur du holder
     * @param peutAlterer Si les vues modifier et supprimer doivent être
     *                    visibles.
     * @param vue L'adaptateur qui gère le holder.
     */
    public HolderTache(@NonNull View vue, boolean peutAlterer, Adaptateur adaptateur) {
        super(vue);
        this.vue = vue;
        this.adaptateur = adaptateur;
        description = vue.findViewById(R.id.descriptionTache);
        valeur = vue.findViewById(R.id.points);
        modifier = vue.findViewById(R.id.modifierTache);
        supprimer = vue.findViewById(R.id.supprimerTache);
        if (!peutAlterer) {
            modifier.setVisibility(View.GONE);
            supprimer.setVisibility(View.GONE);
        }
        else {
            modifierTache();
            supprimer();
        }
    }

    /**
     * Action à faire à l'appuie du bouton modifier
     */
    private void modifierTache() {
        modifier.setOnClickListener(View -> {
        popDialogueModification();
        });
    }

    /**
     * Action à faire à l'appuie du bouton supprimer
     */
    private void supprimer() {
        supprimer.setOnClickListener(View -> {
            adaptateur.ressources.remove(getBindingAdapterPosition());
            adaptateur.notifyItemRemoved(getBindingAdapterPosition());
            GestionFichier.mettreAJourFichier(vue, adaptateur.ressources,
                    "Taches");
        });
    }

    /**
     * Ouvre une boîte de dialogue pour modifier le texte de la description.
     */
    private void popDialogueModification() {
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(vue.getContext());
        builder.setTitle("Changer le nom de la tâche?");

        LayoutInflater inflater = LayoutInflater.from(vue.getContext());
        View layout = inflater.inflate(R.layout.layout_dialogue_tache, null);
        builder.setView(layout);
        final EditText input = layout.findViewById(R.id.champ_nom_tache);
        input.setText(description.getText());

        final EditText input2 = layout.findViewById(R.id.champ_points_tache_ajout);
        input2.setText(valeur.getText());

        builder.setPositiveButton("Modifier",
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                description.setText(input.getText().toString());
                adaptateur.ressources.get(getBindingAdapterPosition())
                        .setdescription(input.getText().toString());
                valeur.setText(input2.getText().toString());
                ((RessourceTache) adaptateur.ressources
                        .get(getBindingAdapterPosition()))
                        .setValeur(input2.getText().toString());
                GestionFichier.mettreAJourFichier(vue, adaptateur.ressources,
                        "Taches");
            }
        });
        builder.setNegativeButton("Annuler",
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    /**
     * Retourne La vue de la description.
     * @return
     */
    public TextView getTextViewDescription() {
        return description;
    }

    /**
     * Retourne la vue de la valeur de la tâche
     * @return
     */
    public TextView getTextViewValeur() {
        return valeur;
    }
}
