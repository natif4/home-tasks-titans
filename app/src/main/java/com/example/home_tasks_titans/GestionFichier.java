package com.example.home_tasks_titans;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.example.home_tasks_titans.Ressources.Ressource;
import com.example.home_tasks_titans.Ressources.RessourceTache;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;

/**
 * @author Jasmin Bergeron-Joubert
 * @version %I%,%G%
 */

/**
 * Classe abstraite pour le traitement des fichiers
 */
public abstract class GestionFichier {

    /**
     * Permet de remplir la liste envoyée avec le contenu du fichier envoyé.
     *
     * @param context Représente l'état du système.
     * @param path Le chemain du fichier envoyé.
     * @param liste La liste à remplir.
     */
    public static void lireListeDansFichier(Context context, String path,
                                            ArrayList<String> liste) {
        String ligne;
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            while ((ligne = br.readLine()) != null) {
                liste.add(ligne);
            }
            br.close();
        } catch (IOException e) {
            Toast.makeText(context, "Le fichier n'existe pas ou la liste " +
                            "est vide",
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Permet d'écrire dans le fichier envoyé le contenu de la liste envoyé.
     *
     * @param context Représente l'état du système.
     * @param path Le chemain du fichier à remplir.
     * @param liste La liste à partir de laquelle on rempli le fichier.
     */
    public static void ecrireListeDansFichier(Context context, String path,
                                              ArrayList<String> liste) {
        try {
            FileWriter fw = new FileWriter(path);
            Writer writer = new BufferedWriter(fw);
            for (String item : liste) {
                writer.write(item + "\n");
            }
            writer.close();
        } catch (IOException e) {
            Toast.makeText(context, "Le fichier n'a pas" +
                    " pu être créer.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Ajoute un élément String à un fichier spécifié.
     * @param vue La vue qui demande l'ajout.
     * @param texte Le texte à enregistrer.
     * @param nomFichier le nom du fichier auquel ajouter le texte.
     * @return La liste enregistrée.
     */
    public static ArrayList<String> ajouterDansFichier(View vue, String texte,
                                                       String nomFichier) {
        ArrayList<String> contenuFichier = new ArrayList<>();
        File fichier = new File(vue.getContext().getFilesDir(),
                nomFichier +".txt");
        GestionFichier.lireListeDansFichier(vue.getContext(),
                fichier.toString(), contenuFichier);

        if (fichier.exists()) {
            fichier.delete();
        }
        try {
            fichier.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        contenuFichier.add(texte);
        GestionFichier.ecrireListeDansFichier(vue.getContext(),
                fichier.toString(), contenuFichier);
        return contenuFichier;
    }

    /**
     * Met à jour le ou les fichiers selon la liste des ressources envoyées.
     * @param vue La vue qui nécessite la mise à jour du ou des fichiers.
     * @param ressources La liste des ressources à mettre dans le ou les
     *                  fichiers.
     * @param typeHolder Sert à savoir quel nom est utilisé pour le fichier.
     *                   Par exemple, pour les descriptions des tâches, on doit
     *                   mettre "Taches".
     */
    public static void mettreAJourFichier(
            View vue, ArrayList<? extends Ressource> ressources,
            String typeHolder) {
        ArrayList<String> descriptions = new ArrayList<>();
        ArrayList<String> valeurs = new ArrayList<>();
        File fichierDescriptionsTache = new File(vue.getContext().getFilesDir(),
                 "descriptions" + typeHolder + ".txt");

        for (int i = 0; i < ressources.size(); i++) {
            descriptions.add(ressources.get(i).getDescription());
            if (ressources.get(i) instanceof RessourceTache) {
                valeurs.add(((RessourceTache) ressources.get(i)).getValeur());
            }
        }
        if (fichierDescriptionsTache.exists()) {
            fichierDescriptionsTache.delete();
        }
        try {
            fichierDescriptionsTache.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        GestionFichier.ecrireListeDansFichier(vue.getContext(),
                fichierDescriptionsTache.toString(), descriptions);
        if (valeurs.size() > 0) {
            File fichiervaleursTache = new File(vue.getContext().getFilesDir(),
                    "valeursTaches.txt");
            if (fichiervaleursTache.exists()) {
                fichiervaleursTache.delete();
            }
            try {
                fichiervaleursTache.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            GestionFichier.ecrireListeDansFichier(vue.getContext(),
                    fichiervaleursTache.toString(), valeurs);
        }
    }
}

