package com.example.home_tasks_titans.Conteneurs;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.home_tasks_titans.Adaptateur;
import com.example.home_tasks_titans.GestionFichier;
import com.example.home_tasks_titans.R;
import com.example.home_tasks_titans.Ressources.RessourceRecompense;

import java.io.File;
import java.util.ArrayList;

/**
 * @author Jasmin Bergeron-Joubert
 * @version %I%,%G%
 */

/**
 * Classe qui s'occupe de la gestion des récompenses.
 */
public class ConteneurRecompense extends Fragment {
    /**
     * La liste de description des récompenses existantes
     */
    private ArrayList<String> descriptionRecompenses;
    /**
     * L'adaptateur qui sert à remlir le recyclerView.
     */
    private Adaptateur adaptateur;

    /**
     * Actions à faire au lancement de l'activité.
     *
     * @param inflater L'objet qui est utilisé pour mettre les vues dans le
     *                fragment.
     * @param container La vue parente.
     * @param savedInstanceState Les informations de l'activité afin de pouvoir
     *                           la relancer dans le même état si nécessaire.
     *
     * @return La vue du conteneur.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.container_recompense, container,
                false);
    }

    /**
     * Actions à faire après la création de l'activité.
     * @param vue La vue du conteneur.
     * @param savedInstanceState les informations de l'activité afin de pouvoir
     *                           la relancer dans le même état si nécessaire.
     */
    @Override
    public void onViewCreated(View vue, Bundle savedInstanceState) {
        super.onViewCreated(vue, savedInstanceState);
        ArrayList<RessourceRecompense> ressourceRecompenses = new ArrayList<>();
        descriptionRecompenses = new ArrayList<>();
        Button ajouterRecompense = vue.findViewById(R.id.boutton_ajouter_recompense);
        ajouterRecompense.setOnClickListener(n -> {
            popDialogue(vue, ressourceRecompenses);
        });
        RemplirListeRecompenses(vue, ressourceRecompenses);
    }

    /**
     * Rempli la liste des RessourcesRecompenses avec le contenus du fichiers
     *      descriptionRecompenses.
     */
    private void RemplirListeRecompenses(View vue, ArrayList<RessourceRecompense> ressourceRecompenses) {
        File fichierDescriptions = new File(vue.getContext().getFilesDir(), "descriptionsRecompenses.txt");
        if (fichierDescriptions.exists()) {
            GestionFichier.lireListeDansFichier(vue.getContext(),
                    fichierDescriptions.toString(), descriptionRecompenses);
            for (int i = 0; i < descriptionRecompenses.size(); i++) {
                ressourceRecompenses.add(new RessourceRecompense(descriptionRecompenses.get(i)));
            }
        }
        initialiseRecyclerView(vue, ressourceRecompenses);
    }

    /**
     * Initialise le recyclerView.
     */
    private void initialiseRecyclerView(View vue, ArrayList<RessourceRecompense> ressourceRecompenses) {
        adaptateur = new Adaptateur(ressourceRecompenses, true);
        RecyclerView recyclerView = vue.findViewById(R.id.liste_recompenses_existante);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(vue.getContext());
        recyclerView.addItemDecoration(new DividerItemDecoration(vue.getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adaptateur);
        recyclerView.setLayoutManager(manager);
    }

    /**
     * Affiche une boîte de dialogue pour entrer le texte de la récompense.
     */
    private void popDialogue(View vue, ArrayList<RessourceRecompense> ressourceRecompenses) {
        AlertDialog.Builder builder = new AlertDialog.Builder(vue.getContext());
        builder.setTitle("Quelle récompense voulez-vous ajouter?");

        LayoutInflater inflater = this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.layout_dialogue_recompense, null);
        builder.setView(layout);
        final EditText description = layout.findViewById(R.id.champ_nom_recompense);

        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        descriptionRecompenses = GestionFichier.ajouterDansFichier(vue,
                                        description.getText().toString(), "descriptionsRecompenses");
                        ressourceRecompenses.add(new RessourceRecompense(descriptionRecompenses
                                .get(descriptionRecompenses.size()-1)));
                        adaptateur.notifyItemInserted(ressourceRecompenses.size()-1);
                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.show();
    }
}
