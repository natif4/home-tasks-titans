package com.example.home_tasks_titans.Conteneurs;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.home_tasks_titans.Activities.ActivityPartie;
import com.example.home_tasks_titans.Adaptateur;
import com.example.home_tasks_titans.GestionFichier;
import com.example.home_tasks_titans.R;
import com.example.home_tasks_titans.Ressources.RessourceRecompense;
import com.example.home_tasks_titans.Ressources.RessourceTache;

import java.io.File;
import java.util.ArrayList;

/**
 * @author Jasmin Bergeron-Joubert
 * @version %I%,%G%
 */

/**
 * Classe qui s'occupe de la gestion du setup de la partie.
 */
public class ConteneurPreparationPartie extends Fragment {

    /**
     * La liste des numéros des Drawables monstres à affronter
     */
    private int[] monstres;
    /**
     * Le numero du Drawable du monstre affiché.
     */
    private int numeroMonstre;
    /**
     * Les ressourceTaches qui ont été sélectionnées pour la partie.
     */
    private ArrayList<RessourceTache> tachesSelectionnees;
    /**
     * Les ressourceRecompenses qui ont été sélectionnées pour la partie.
     */
    private ArrayList<RessourceRecompense> recompensesSelectionnees;

    /**
     * Actions à faire au lancement de l'activité.
     *
     * @param inflater           L'objet qui est utilisé pour mettre les vues dans le
     *                           fragment.
     * @param container          La vue parente.
     * @param savedInstanceState Les informations de l'activité afin de pouvoir
     *                           la relancer dans le même état si nécessaire.
     * @return La vue du conteneur.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.container_setup_partie, container, false);
    }

    /**
     * Actions à faire après la création de l'activité.
     *
     * @param vue                La vue du conteneur.
     * @param savedInstanceState les informations de l'activité afin de pouvoir
     *                           la relancer dans le même état si nécessaire.
     */
    @Override
    public void onViewCreated(View vue, Bundle savedInstanceState) {
        super.onViewCreated(vue, savedInstanceState);
        monstres = new int[]{R.drawable.djsalistou, R.drawable.baveux, R.drawable.informonstricien, R.drawable.mort,
                R.drawable.pieuvreinfernale, R.drawable.yetidelapropreteici, R.drawable.zombie};
        numeroMonstre = 0;
        tachesSelectionnees = new ArrayList<>();
        recompensesSelectionnees = new ArrayList<>();
        preparationVues(vue);
    }

    /**
     * Prépare les différents ImageViews et boutons du conteneur.
     *
     * @param vue     La vue du conteneur.
     */
    private void preparationVues(View vue) {
        ImageView monstre = vue.findViewById(R.id.monstre);
        monstre.setImageResource(monstres[numeroMonstre]);

        ImageView monstrePrecedent = vue.findViewById(R.id.monstre_precedent);
        monstrePrecedent.setOnClickListener(view -> {
            reculerDansListeMonstre();
            monstre.setImageResource(monstres[numeroMonstre]);
        });

        ImageView monstreSuivant = vue.findViewById(R.id.monstre_suivant);
        monstreSuivant.setOnClickListener(view -> {
            avancerDansListeMonstre();
            monstre.setImageResource(monstres[numeroMonstre]);
        });

        Button lancer = vue.findViewById(R.id.lancer_partie);
        lancer.setOnClickListener(view -> {
            if (recompensesSelectionnees.size() > 0 && tachesSelectionnees.size() > 0) {
                popDialogueCodeConfirmation(vue);
            }else {
                Toast.makeText(vue.getContext(), "La partie a besoin d'au moins une tâche et une récompense.",
                        Toast.LENGTH_SHORT).show();
            }
        });

        Button boutonTaches = vue.findViewById(R.id.bouton_selection_taches);
        boutonTaches.setOnClickListener(view -> {
            popDialogueChoixTaches(vue);
        });

        Button boutonRecompenses = vue.findViewById(R.id.bouton_selection_recompenses);
        boutonRecompenses.setOnClickListener(view -> {
            popDialogueChoixRecompenses(vue);
        });
    }

    /**
     * Permet de changer d'image de monstre vers le début de la liste.
     */
    private void reculerDansListeMonstre() {
        if (numeroMonstre == 0) {
            numeroMonstre = monstres.length - 1;
        } else {
            numeroMonstre--;
        }
    }

    /**
     * Permet de changer d'image de monstre vers la fin de la liste.
     */
    private void avancerDansListeMonstre() {
        if (numeroMonstre == monstres.length - 1) {
            numeroMonstre = 0;
        } else {
            numeroMonstre++;
        }
    }

    /**
     * Affiche la fenêtre de choix des tâches à ajouter dans la partie.
     *
     * @param vue La vue du conteneur
     */
    private void popDialogueChoixTaches(View vue) {
        ArrayList<RessourceTache> ressourceTaches = new ArrayList<>();
        RemplirListeTaches(vue, ressourceTaches);
        AlertDialog.Builder builder = new AlertDialog.Builder(vue.getContext());
        builder.setTitle("Sélectionnez les tâches à faire : ");
        LayoutInflater inflater = this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.layout_dialogue_liste, null);

        Adaptateur adaptateur =
                new Adaptateur(ressourceTaches, false, true, tachesSelectionnees, recompensesSelectionnees);
        RecyclerView recyclerView = layout.findViewById(R.id.liste_dialogue);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(vue.getContext());
        recyclerView.addItemDecoration(new DividerItemDecoration(vue.getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adaptateur);
        recyclerView.setLayoutManager(manager);

        builder.setView(layout);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        initialiseRecyclerViewTaches(vue);
                        dialog.cancel();
                    }
                });
        builder.show();
    }

    /**
     * Rempli la liste des RessourceTaches avec le contenus des fichiers descriptionsTaches.txt et valeursTaches.txt.
     *
     * @param vue La vue du conteneur
     */
    private void RemplirListeTaches(View vue, ArrayList<RessourceTache> ressourceTaches) {
        ArrayList<String> descriptionTaches = new ArrayList<>();
        ArrayList<String> valeurTaches = new ArrayList<>();
        File fichierDescriptions = new File(vue.getContext().getFilesDir(), "descriptionsTaches.txt");
        File fichierValeurs = new File(vue.getContext().getFilesDir(), "valeursTaches.txt");

        if (fichierDescriptions.exists() && fichierValeurs.exists()) {
            GestionFichier.lireListeDansFichier(vue.getContext(), fichierDescriptions.toString(), descriptionTaches);
            GestionFichier.lireListeDansFichier(vue.getContext(), fichierValeurs.toString(), valeurTaches);
            for (int i = 0; i < descriptionTaches.size(); i++) {
                ressourceTaches.add(new RessourceTache(descriptionTaches.get(i), (valeurTaches.get(i))));
            }
        }
    }

    /**
     * Initialise le recyclerView des tâches.
     *
     * @param vue La vue du conteneur.
     */
    private void initialiseRecyclerViewTaches(View vue) {
        Adaptateur adaptateurTache =
                new Adaptateur(tachesSelectionnees, true);
        RecyclerView recyclerView = vue.findViewById(R.id.taches_choisies);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(vue.getContext());
        recyclerView.addItemDecoration(new DividerItemDecoration(vue.getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adaptateurTache);
        recyclerView.setLayoutManager(manager);
    }

    /**
     * Affiche la fenêtre de choix des récompenses à ajouter dans la partie.
     *
     * @param vue La vue du conteneur
     */
    private void popDialogueChoixRecompenses(View vue) {
        ArrayList<RessourceRecompense> ressourceRecompenses = new ArrayList<>();
        RemplirListeRecompenses(vue, ressourceRecompenses);
        AlertDialog.Builder builder = new AlertDialog.Builder(vue.getContext());
        builder.setTitle("Sélectionnez les tâches à faire : ");
        LayoutInflater inflater = this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.layout_dialogue_liste, null);

        Adaptateur adaptateur =
                new Adaptateur(ressourceRecompenses, false, true, tachesSelectionnees, recompensesSelectionnees);
        RecyclerView recyclerView = layout.findViewById(R.id.liste_dialogue);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(vue.getContext());
        recyclerView.addItemDecoration(new DividerItemDecoration(vue.getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adaptateur);
        recyclerView.setLayoutManager(manager);

        builder.setView(layout);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        initialiseRecyclerViewRecompenses(vue);
                        dialog.cancel();
                    }
                });
        builder.show();
    }


    /**
     * Rempli la liste des RessourcesRecompenses avec le contenus du fichiers descriptionsRecompenses.txt.
     *
     * @param vue La vue du conteneur
     */
    private void RemplirListeRecompenses(View vue, ArrayList<RessourceRecompense> ressourceRecompenses) {
        ArrayList<String> descriptionRecompenses = new ArrayList<>();
        File fichierDescriptions = new File(vue.getContext().getFilesDir(), "descriptionsRecompenses.txt");
        if (fichierDescriptions.exists()) {
            GestionFichier.lireListeDansFichier(vue.getContext(),
                    fichierDescriptions.toString(), descriptionRecompenses);
            for (int i = 0; i < descriptionRecompenses.size(); i++) {
                ressourceRecompenses.add(new RessourceRecompense(descriptionRecompenses.get(i)));
            }
        }
    }

    /**
     * Initialise le recyclerView des recompenses.
     *
     * @param vue La vue du conteneur
     */
    private void initialiseRecyclerViewRecompenses(View vue) {
        Adaptateur adaptateurRecompense =
                new Adaptateur(recompensesSelectionnees, true);
        RecyclerView recyclerView = vue.findViewById(R.id.recompenses_choisies);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(vue.getContext());
        recyclerView.addItemDecoration(new DividerItemDecoration(vue.getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adaptateurRecompense);
        recyclerView.setLayoutManager(manager);
    }



    /**
     * Prépare et lance l'ActivityPartie.
     *
     * @param vue              La vue du conteneur.
     * @param codeConfirmation Le code de confirmation pour attester que les tâches sont bien complétées.
     */
    private void lancerPartie(View vue, String codeConfirmation) {
        Intent intent = new Intent(vue.getContext(), ActivityPartie.class);
        intent.putExtra("monstre", numeroMonstre);
        intent.putExtra("code", codeConfirmation);
        intent.putParcelableArrayListExtra("ressourceTachesSelectionnees", tachesSelectionnees);
        intent.putParcelableArrayListExtra("ressourceRecompenseSelectionnees", recompensesSelectionnees);
        startActivity(intent);
    }

    /**
     * Affiche une boîte de dialogue pour entrer le code de confirmation des tâches.
     *
     * @param vue La vue du conteneur
     */
    private void popDialogueCodeConfirmation(View vue) {
        AlertDialog.Builder builder = new AlertDialog.Builder(vue.getContext());
        builder.setTitle("Entrez le code de confirmation des tâches:");
        LayoutInflater inflater = this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.layout_dialogue_creer_code, null);

        final EditText code = layout.findViewById(R.id.creation_code_input1);
        final EditText confirmationCode = layout.findViewById(R.id.creation_code_input2);

        builder.setView(layout);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (code.getText().toString().equals(confirmationCode.getText().toString())) {
                    lancerPartie(vue, code.getText().toString());
                } else {
                    Toast.makeText(vue.getContext(), "Les codes entrés ne sont pas identiques",
                            Toast.LENGTH_SHORT).show();
                    popDialogueCodeConfirmation(vue);
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
}
