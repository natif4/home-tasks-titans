package com.example.home_tasks_titans.Conteneurs;


import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.home_tasks_titans.Adaptateur;
import com.example.home_tasks_titans.GestionFichier;
import com.example.home_tasks_titans.R;
import com.example.home_tasks_titans.Ressources.RessourceTache;

import java.io.File;
import java.util.ArrayList;

/**
 * @author Jasmin Bergeron-Joubert
 * @version %I%,%G%
 */

/**
 * Classe qui s'occupe de la gestion des tâches.
 */
public class ConteneurTache extends Fragment {

    /**
     * La liste de description des tâches existantes.
     */
    private ArrayList<String> descriptionTaches;
    /**
     * Les points que valent les tâches existantes.
     */
    private ArrayList<String> valeurTaches;
    /**
     * l'adaptateur qui sert à remplir le RecyclerView
     */
    private Adaptateur adaptateur;


    /**
     * Actions à faire au lancement de l'activité.
     *
     * @param inflater L'objet qui est utilisé pour mettre les vues dans le
     *                fragment.
     * @param container La vue parente.
     * @param savedInstanceState Les informations de l'activité afin de pouvoir
     *                           la relancer dans le même état si nécessaire.
     *
     * @return La vue du conteneur.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.container_tache, container,
                false);
    }


    /**
     * Actions à faire après la création de l'activité.
     *
     * @param vue La vue du conteneur.
     * @param savedInstanceState les informations de l'activité afin de pouvoir
     *                           la relancer dans le même état si nécessaire.
     */
    @Override
    public void onViewCreated(View vue, Bundle savedInstanceState) {
        super.onViewCreated(vue, savedInstanceState);
        ArrayList<RessourceTache> ressourceTaches = new ArrayList<>();
        descriptionTaches = new ArrayList<>();
        valeurTaches = new ArrayList<>();

        Button ajouterTache = vue.findViewById(R.id.bouton_ajouter_tache);
        ajouterTache.setOnClickListener(n -> {
            popDialogue(vue, ressourceTaches);
        });

        RemplirListeTaches(vue, ressourceTaches);
    }

    /**
     * Rempli la liste des RessourceTaches avec le contenus des fichiers
     * descriptionTaches et valeursTaches
     */
    private void RemplirListeTaches(View vue, ArrayList<RessourceTache> ressourceTaches) {
        descriptionTaches.clear();
        valeurTaches.clear();
        File fichierDescriptions = new File(vue.getContext().getFilesDir(),"descriptionsTaches.txt");
        File fichierValeurs = new File(vue.getContext().getFilesDir(), "valeursTaches.txt");

        if (fichierDescriptions.exists() && fichierValeurs.exists()) {
            GestionFichier.lireListeDansFichier(vue.getContext(), fichierDescriptions.toString(), descriptionTaches);
            GestionFichier.lireListeDansFichier(vue.getContext(), fichierValeurs.toString(), valeurTaches);
            for (int i = 0; i < descriptionTaches.size(); i++) {
                ressourceTaches.add(new RessourceTache(descriptionTaches.get(i),
                        (valeurTaches.get(i))));
            }
        }
        initialiseRecyclerView(vue, ressourceTaches);
    }

    /**
     * Initialise le recyclerView
     */
    private void initialiseRecyclerView(View vue, ArrayList<RessourceTache> ressourceTaches) {
        adaptateur = new Adaptateur(ressourceTaches, true);
        RecyclerView recyclerView = vue.findViewById(R.id.liste_tache_deja_cree);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(vue.getContext());
        recyclerView.addItemDecoration(new DividerItemDecoration(vue.getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adaptateur);
        recyclerView.setLayoutManager(manager);
    }

    /**
     * Affiche une boîte de dialogue pour entrer le texte de la tâche et sa valeur.
     */
    private void popDialogue(View vue, ArrayList<RessourceTache> ressourceTaches) {
        AlertDialog.Builder builder = new AlertDialog.Builder(vue.getContext());
        builder.setTitle("Quelle tâche voulez-vous ajouter?");

        LayoutInflater inflater = this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.layout_dialogue_tache, null);
        builder.setView(layout);
        final EditText description = layout.findViewById(R.id.champ_nom_tache);
        final EditText valeur = layout.findViewById(R.id.champ_points_tache_ajout);



        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            Integer.parseInt(valeur.getText().toString());
                            descriptionTaches = GestionFichier.ajouterDansFichier(vue,
                                    description.getText().toString(), "descriptionsTaches");
                            valeurTaches = GestionFichier.ajouterDansFichier(vue,
                                    valeur.getText().toString(), "valeursTaches");
                            ressourceTaches.add(new RessourceTache(descriptionTaches.get(descriptionTaches.size() - 1),
                                    valeurTaches.get(valeurTaches.size() - 1)));
                            adaptateur.notifyItemInserted(ressourceTaches.size() - 1);
                        } catch (Exception e){
                            Toast.makeText(vue.getContext(), "La valeur doit être un nombre.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.show();
    }
}
