package com.example.home_tasks_titans.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.home_tasks_titans.Adaptateur;
import com.example.home_tasks_titans.R;
import com.example.home_tasks_titans.Ressources.RessourceRecompense;
import com.example.home_tasks_titans.Ressources.RessourceTache;

import java.util.ArrayList;

/**
 * @author Jasmin Bergeron-Joubert
 * @version %I%,%G%
 */

/**
 * Classe de gestion de la partie
 */
public class ActivityPartie extends AppCompatActivity {

    /**
     * La liste des tâche à accomplir pour le jeu.
     */
    private ArrayList<RessourceTache> ressourceTaches;

    /**
     * La liste des récompenses disponibles à la réussite du jeu.
     */
    private ArrayList<RessourceRecompense> ressourceRecompenses;

    /**
     * Le niveau de peur actuel du monstre en fonction de la valeur des tâches accomplies et le total des valeurs
     * des tâches à faire.
     */
    private int peurActuelle, peurMaximum;

    /**
     * Le code de confirmation pour la completion de la tâche demandé et l'affichage de base pour l'indicateur de peur.
     */
    private String codeConfirmation, texteBaseAffichagePeur;

    /**
     * L'adaptateur des tâches
     */
    private Adaptateur adaptateur;

    /**
     * Le texte qui affiche la peur actuelle du monstre et la peur total requise pour le faire fuir.
     */
    private TextView peur;

    /**
     * La barre qui affiche l'intensité de peur du monstre
     */
    private ProgressBar intensitePeur;

    /**
     * La position de la tâche sélectionnée.
     * Choisi de prendre une liste afin de pouvoir envoyer l'objet et le changer par l'enfant.
     */
    private ArrayList<Integer> positionSelectionnee;

    /**
     * Vrai si le code de confirmation est valide.
     */
    private boolean codeConfirme;

    /**
     * L'image du monstre de la partie
     */
    private ImageView monstreAFaireFuir;

    /**
     * Les animations utilisées dan la partie
     */
    private Animation animMonstreTremble, animMonstreDisparait;


    /**
     * Actions à faire à la création de l'activité.
     * @param savedInstanceState les informations de l'activité afin de pouvoir
     *      *                    la relancer dans le même état si nécessaire.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partie);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        preparationInitiale();
        preparationAnimation();
        initialiseRecyclerViewTaches();
        texteBaseAffichagePeur = peur.getText().toString();
        rafraichirPeurActuelle();
    }

    /**
     * Fait la préparation des valeurs et des vues au lancement de l'activité.
     */
    private void preparationInitiale() {
        int[] monstres = new int[]{R.drawable.djsalistou, R.drawable.baveux, R.drawable.informonstricien,
                R.drawable.mort, R.drawable.pieuvreinfernale, R.drawable.yetidelapropreteici, R.drawable.zombie};

        ressourceRecompenses = getIntent().getParcelableArrayListExtra("ressourceRecompenseSelectionnees");
        ressourceTaches = getIntent().getParcelableArrayListExtra("ressourceTachesSelectionnees");
        int monstre = getIntent().getIntExtra("monstre", 0);
        codeConfirmation = getIntent().getExtras().getString("code");

        monstreAFaireFuir = findViewById(R.id.monstre_a_faire_fuir);
        monstreAFaireFuir.setImageResource(monstres[monstre]);

        intensitePeur = findViewById(R.id.barre_intensité_de_peur);
        peur = findViewById(R.id.niveau_de_peur);
        codeConfirme = false;
        peurMaximum = 0;
        peurActuelle = 0;
        for (RessourceTache tache : ressourceTaches) {
            peurMaximum += Integer.parseInt(tache.getValeur());
        }
        intensitePeur.setMax(peurMaximum);
        intensitePeur.setIndeterminate(false);
        positionSelectionnee = new ArrayList<>();
        Button bouton = findViewById(R.id.bouton_faire_peur);
        bouton.setOnClickListener(view -> {
            if (positionSelectionnee.size() == 1) {
                popDialogueCodeConfirmation();
            }
        });
    }

    /**
     * Fait la préparation des animations de la partie.
     */
    private void preparationAnimation() {
        animMonstreTremble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
        animMonstreDisparait = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.disparait_a_gauche);
    }

    /**
     * Action à effectuer quand une tâche est validée
     */
    private void fairePeurAuMonstre() {
        if (codeConfirme) {
            MediaPlayer sons;
            peurActuelle += Integer.parseInt(ressourceTaches.get(positionSelectionnee.get(0)).getValeur());
            rafraichirPeurActuelle();
            codeConfirme = false;
            int ressourceAEnlever = positionSelectionnee.get(0);

            adaptateur.ressources.remove(ressourceAEnlever);
            adaptateur.notifyItemRemoved(ressourceAEnlever);
            positionSelectionnee.clear();

            if (peurActuelle == peurMaximum) {
                sons = MediaPlayer.create(ActivityPartie.this, R.raw.swinging_staff_whoosh_strong_08);
                sons.start();
                monstreAFaireFuir.startAnimation(animMonstreDisparait);
                monstreAFaireFuir.setVisibility(View.INVISIBLE);
                popDialogueRecompenses();
            } else {
                sons = MediaPlayer.create(ActivityPartie.this, R.raw.ahh);
                sons.start();
                monstreAFaireFuir.startAnimation(animMonstreTremble);
            }
        }
    }

    /**
     * Actualise l'affichage des vues de peur du  monstre et de la barre de progression.
     */
    private void rafraichirPeurActuelle() {
        peur.setText(texteBaseAffichagePeur + " " + peurActuelle + " / " + peurMaximum);
        intensitePeur.setProgress(peurActuelle);
    }

    /**
     * Initialise le RecyclerView des tâches avec ses vues initiales.
     */
    private void initialiseRecyclerViewTaches() {
        adaptateur = new Adaptateur(ressourceTaches, false, false, positionSelectionnee);
        RecyclerView recyclerView = this.findViewById(R.id.taches_a_accomplir);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adaptateur);
        recyclerView.setLayoutManager(manager);
    }

    /**
     * Fait apparaitre la fenêtre d'entrée du code de confirmation.
     */
    private void popDialogueCodeConfirmation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Entrez le code de confirmation: ");

        LayoutInflater inflater = this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.layout_dialogue_code_confirmer_tache, null);
        builder.setView(layout);
        final EditText input = layout.findViewById(R.id.code_confirmation_tache_faite);

        builder.setView(layout);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String codeEntre = input.getText().toString();
                        if (codeEntre.equals(codeConfirmation)) {
                            codeConfirme = true;
                            fairePeurAuMonstre();
                        } else {
                            Toast.makeText(getBaseContext(), "Code erroné!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.show();
    }

    /**
     * Fait apparaitre la fenêtre avec la liste des récompenses permises.
     */
    private void popDialogueRecompenses() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Bravo! Le monstre a trop peur et il est enfuit! Choisi ta récompense : ");
        LayoutInflater inflater = this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.layout_dialogue_liste, null);

        Adaptateur adaptateurRecompenses = new Adaptateur(ressourceRecompenses, false);
        RecyclerView recyclerView = layout.findViewById(R.id.liste_dialogue);
        RecyclerView.LayoutManager managerRecompense = new LinearLayoutManager(this);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adaptateurRecompenses);
        recyclerView.setLayoutManager(managerRecompense);


        builder.setView(layout);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getBaseContext(), MainActivity.class);
                        startActivity(intent);
                    }
                });
        builder.show();
    }
}
