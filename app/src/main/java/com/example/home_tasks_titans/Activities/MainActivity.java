package com.example.home_tasks_titans.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.example.home_tasks_titans.Conteneurs.ConteneurRecompense;
import com.example.home_tasks_titans.Conteneurs.ConteneurPreparationPartie;
import com.example.home_tasks_titans.Conteneurs.ConteneurTache;
import com.example.home_tasks_titans.R;
import com.google.android.material.tabs.TabLayout;

/**
 * @author Jasmin Bergeron-Joubert
 * @version %I%,%G%
 */

/**
 * Activité de gestion des éléments persistants et de la préparation de la partie.
 */
public class MainActivity extends AppCompatActivity
        implements TabLayout.OnTabSelectedListener {

    /**
     * La barre qui contient les onglets
     */
    private TabLayout barreOnglets;

    /**
     * Actions à faire à la création de l'activité.
     * @param savedInstanceState les informations de l'activité afin de pouvoir
     *      *                    la relancer dans le même état si nécessaire.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        barreOnglets = findViewById(R.id.main_activity_tabBar);
        barreOnglets.addOnTabSelectedListener(this);
    }

    /**
     * Actions à faire à la sélection d'un onglet
     *
     * @param tab l'onglet sélectionné
     */
    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        Fragment fragment = null;
        if (barreOnglets.getSelectedTabPosition() == 0) {
            fragment = new ConteneurTache();
        } else if (barreOnglets.getSelectedTabPosition() == 1) {
            fragment = new ConteneurRecompense();
        } else if (barreOnglets.getSelectedTabPosition() == 2) {
            fragment = new ConteneurPreparationPartie();
        }
        if (fragment != null) {
            FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentContainerView, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    /**
     * Actions à faire au relâchement d'un onglet
     *
     * @param tab l'onglet relâché
     */
    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }
    /**
     * Actions à faire au resélection d'un onglet
     *
     * @param tab l'onglet resélectionné
     */
    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}