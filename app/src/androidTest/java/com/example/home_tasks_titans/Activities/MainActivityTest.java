package com.example.home_tasks_titans.Activities;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.RootMatchers.isDialog;
import static androidx.test.espresso.matcher.ViewMatchers.hasChildCount;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import android.content.Context;
import android.os.Looper;
import android.view.View;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.home_tasks_titans.GestionFichier;
import com.example.home_tasks_titans.R;

import org.hamcrest.Matcher;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.File;
import java.util.ArrayList;

/**
 * @author Jasmin Bergeron-Joubert
 * @version %I%,%G%
 */
@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MainActivityTest {

    @Rule
    public ActivityScenarioRule<MainActivity> activityRule = new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void onTabSelectedTab1() {
        onView(withText("Tâches")).perform(click());
        onView(withId(R.id.bouton_ajouter_tache)).check(matches(isDisplayed()));
    }

    @Test
    public void onTabSelectedTab2() {
        onView(withText("Récompenses")).perform(click());
        onView(withId(R.id.boutton_ajouter_recompense)).check(matches(isDisplayed()));
    }

    @Test
    public void onTabSelectedTab3() {
        onView(withText("Préparation de la partie")).perform(click());
        onView(withId(R.id.lancer_partie)).check(matches(isDisplayed()));
    }

    @Test
    public void ajouterTache() {
        ArrayList<String> descriptionsTaches = new ArrayList<>();
        Context context = ApplicationProvider.getApplicationContext();
        File fichier = new File(context.getFilesDir(), "descriptionsTaches.txt");
        //Looper.prepare(); // Decommenter si pour faire ce test seulement. Besoin de une fois pour tout le fichier
        GestionFichier.lireListeDansFichier(context, fichier.toString(), descriptionsTaches);
        int nombreItemsAvantAjout = descriptionsTaches.size();
        int nombreItemsApresAjout = nombreItemsAvantAjout + 1;

        onView(withText("Tâches")).perform(click());
        onView(withId(R.id.liste_tache_deja_cree)).check(matches(hasChildCount(nombreItemsAvantAjout)));
        onView(withId(R.id.bouton_ajouter_tache)).perform(click());
        onView(withId(R.id.dialogue_tache)).check(matches(isDisplayed()));
        onView(withId(R.id.champ_nom_tache)).perform(replaceText("Une tâche quelconque"));
        onView(withId(R.id.champ_points_tache_ajout)).perform(replaceText("1000"));
        onView(withText("OK")).perform(click());
        onView(withId(R.id.liste_tache_deja_cree)).check(matches(hasChildCount(nombreItemsApresAjout)));
    }

    @Test
    public void ajouterRecompense() {
        ArrayList<String> descriptionsRecompenses = new ArrayList<>();
        Context context = ApplicationProvider.getApplicationContext();
        File fichier = new File(context.getFilesDir(), "descriptionsRecompenses.txt");
        Looper.prepare();
        GestionFichier.lireListeDansFichier(context, fichier.toString(), descriptionsRecompenses);
        int nombreItemsAvantAjout = descriptionsRecompenses.size();
        int nombreItemsApresAjout = nombreItemsAvantAjout + 1;

        onView(withText("Récompenses")).perform(click());
        onView(withId(R.id.liste_recompenses_existante)).check(matches(hasChildCount(nombreItemsAvantAjout)));
        onView(withId(R.id.boutton_ajouter_recompense)).perform(click());
        onView(withId(R.id.dialogue_recompense)).check(matches(isDisplayed()));
        onView(withId(R.id.champ_nom_recompense)).perform(replaceText("Une récompense quelconque"));
        onView(withText("OK")).perform(click());
        onView(withId(R.id.liste_recompenses_existante)).check(matches(hasChildCount(nombreItemsApresAjout)));
    }

    @Test
    public void modifierTache() {
        onView(withText("Tâches")).perform(click());
        onView(withId(R.id.liste_tache_deja_cree)).check(matches(isDisplayed()));
        onView(withId(R.id.liste_tache_deja_cree)).perform(actionOnItemAtPosition(0,
            new ViewAction() {
                @Override
                public Matcher<View> getConstraints() {
                    return null;
                }

                @Override
                public String getDescription() {
                    return "Click on specific button";
                }

                @Override
                public void perform(UiController uiController, View view) {
                    View button = view.findViewById(R.id.modifierTache);
                    // Maybe check for null
                    button.performClick();
                }
            })
        );
        onView(withId(R.id.dialogue_tache)).inRoot(isDialog()).check(matches(isDisplayed()));
        onView(withId(R.id.champ_nom_tache)).inRoot(isDialog()).perform(replaceText("Tâche modifiée!"));
        onView(withText("Modifier")).inRoot(isDialog()).perform(click());
        onView(withText("Tâche modifiée!")).check(matches(isDisplayed()));
    }

    @Test
    public void modifierRecompense() {
        onView(withText("Récompenses")).perform(click());
        onView(withId(R.id.liste_recompenses_existante)).check(matches(isDisplayed()));
        onView(withId(R.id.liste_recompenses_existante)).perform(actionOnItemAtPosition(0,
                new ViewAction() {
                    @Override
                    public Matcher<View> getConstraints() {
                        return null;
                    }

                    @Override
                    public String getDescription() {
                        return "Click on specific button";
                    }

                    @Override
                    public void perform(UiController uiController, View view) {
                        View button = view.findViewById(R.id.modifierRecompense);
                        // Maybe check for null
                        button.performClick();
                    }
                })
        );
        onView(withId(R.id.dialogue_recompense)).inRoot(isDialog()).check(matches(isDisplayed()));
        onView(withId(R.id.champ_nom_recompense)).inRoot(isDialog()).perform(replaceText("Récompense modifiée!"));
        onView(withText("Modifier")).inRoot(isDialog()).perform(click());
        onView(withText("Récompense modifiée!")).check(matches(isDisplayed()));
    }

    @Test
    public void supprimerTache() throws InterruptedException {
        ArrayList<String> descriptionsTaches = new ArrayList<>();
        Context context = ApplicationProvider.getApplicationContext();
        File fichier = new File(context.getFilesDir(), "descriptionsTaches.txt");
        //Looper.prepare(); // Decommenter si pour faire ce test seulement. Besoin de une fois pour tout le fichier
        GestionFichier.lireListeDansFichier(context, fichier.toString(), descriptionsTaches);
        int nombreItemsAvantSuppression = descriptionsTaches.size();
        int nombreItemsApresSuppression = nombreItemsAvantSuppression - 1;

        onView(withText("Tâches")).perform(click());
        onView(withId(R.id.liste_tache_deja_cree)).check(matches(hasChildCount(nombreItemsAvantSuppression)));
        onView(withId(R.id.liste_tache_deja_cree)).perform(actionOnItemAtPosition(0,
            new ViewAction() {
                @Override
                public Matcher<View> getConstraints() {
                    return null;
                }

                @Override
                public String getDescription() {
                    return "Click on specific button";
                }

                @Override
                public void perform(UiController uiController, View view) {
                    View button = view.findViewById(R.id.supprimerTache);
                    // Maybe check for null
                    button.performClick();
                }
            })
        );
        Thread.sleep(1000);
        onView(withId(R.id.liste_tache_deja_cree)).check(matches(hasChildCount(nombreItemsApresSuppression)));
    }

    @Test
    public void supprimerRecompense() throws InterruptedException {
        ArrayList<String> descriptionsRecompenses = new ArrayList<>();
        Context context = ApplicationProvider.getApplicationContext();
        File fichier = new File(context.getFilesDir(), "descriptionsRecompenses.txt");
        //Looper.prepare(); // Decommenter si pour faire ce test seulement. Besoin de une fois pour tout le fichier
        GestionFichier.lireListeDansFichier(context, fichier.toString(), descriptionsRecompenses);
        int nombreItemsAvantSuppression = descriptionsRecompenses.size();
        int nombreItemsApresSuppression = nombreItemsAvantSuppression - 1;

        onView(withText("Récompenses")).perform(click());
        onView(withId(R.id.liste_recompenses_existante)).check(matches(hasChildCount(nombreItemsAvantSuppression)));
        onView(withId(R.id.liste_recompenses_existante)).perform(actionOnItemAtPosition(0,
                new ViewAction() {
                    @Override
                    public Matcher<View> getConstraints() {
                        return null;
                    }

                    @Override
                    public String getDescription() {
                        return "Click on specific button";
                    }

                    @Override
                    public void perform(UiController uiController, View view) {
                        View button = view.findViewById(R.id.supprimerRecompense);
                        // Maybe check for null
                        button.performClick();
                    }
                })
        );
        Thread.sleep(1000);
        onView(withId(R.id.liste_recompenses_existante)).check(matches(hasChildCount(nombreItemsApresSuppression)));
    }

    @Test
    public void LancerPartieImpossibleSansTacheEtRecompense() {
        onView(withText("Préparation de la partie")).perform(click());
        onView(withId(R.id.lancer_partie)).check(matches(isDisplayed()));
        onView(withId(R.id.lancer_partie)).perform(click());

        onView(withId(R.id.lancer_partie)).check(matches(isDisplayed()));
    }

    @Test
    public void lancerPartie() {
        onView(withText("Préparation de la partie")).perform(click());
        onView(withId(R.id.lancer_partie)).check(matches(isDisplayed()));

        onView(withId(R.id.bouton_selection_taches)).perform(click());
        onView(withId(R.id.liste_dialogue)).perform(actionOnItemAtPosition(0, click()));
        onView(withText("OK")).perform(click());

        onView(withId(R.id.bouton_selection_recompenses)).perform(click());
        onView(withId(R.id.liste_dialogue)).perform(actionOnItemAtPosition(0, click()));
        onView(withText("OK")).perform(click());

        onView(withId(R.id.lancer_partie)).perform(click());

        onView(withId(R.id.creation_code)).check(matches(isDisplayed()));
        onView(withId(R.id.creation_code_input1)).perform(replaceText("7"));
        onView(withId(R.id.creation_code_input2)).perform(replaceText("7"));
        onView(withText("OK")).perform(click());

        onView(withId(R.id.bouton_faire_peur)).check(matches(isDisplayed()));
    }
}