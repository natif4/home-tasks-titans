package com.example.home_tasks_titans.Activities;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.RootMatchers.isDialog;
import static androidx.test.espresso.matcher.ViewMatchers.hasChildCount;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import android.content.Intent;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.home_tasks_titans.R;
import com.example.home_tasks_titans.Ressources.RessourceRecompense;
import com.example.home_tasks_titans.Ressources.RessourceTache;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;

/**
 * @author Jasmin Bergeron-Joubert
 * @version %I%,%G%
 */
@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ActivityPartieTest {

    static Intent intent;
    static  {
        ArrayList<RessourceTache> tachesSelectionnees = new ArrayList<>();
        ArrayList<RessourceRecompense> recompensesSelectionnees = new ArrayList<>();
        intent = new Intent(ApplicationProvider.getApplicationContext(), ActivityPartie.class);
        intent.putExtra("monstre", 5);
        intent.putExtra("code", "7");
        tachesSelectionnees.add(new RessourceTache("salon", "1000"));
        tachesSelectionnees.add(new RessourceTache("salle de bain", "1000"));
        intent.putParcelableArrayListExtra("ressourceTachesSelectionnees", tachesSelectionnees);
        recompensesSelectionnees.add(new RessourceRecompense("30 minutes de télé"));
        recompensesSelectionnees.add(new RessourceRecompense("30 minutes de jeux vidéo"));
        intent.putParcelableArrayListExtra("ressourceRecompenseSelectionnees", recompensesSelectionnees);
    }

    @Rule
    public ActivityScenarioRule<ActivityPartie> activityRule = new ActivityScenarioRule<>(intent);

    @Test
    public void confirmerTacheFaite() {
        onView(withId(R.id.monstre_a_faire_fuir)).check(matches(isDisplayed()));
        onView(withId(R.id.taches_a_accomplir)).check(matches(hasChildCount(2)));
        onView(withId(R.id.taches_a_accomplir)).perform(actionOnItemAtPosition(0, click()));
        onView(withId(R.id.bouton_faire_peur)).perform(click());
        onView(withId(R.id.code_confirmation_tache_faite)).perform(replaceText("7"));
        onView(withText("OK")).perform(click());
        onView(withId(R.id.niveau_de_peur)).check(matches(withText("Intensité de peur du monstre: 1000 / 2000")));
        onView(withId(R.id.taches_a_accomplir)).check(matches(hasChildCount(1)));
    }

    @Test
    public void terminerPartie() {
        onView(withId(R.id.monstre_a_faire_fuir)).check(matches(isDisplayed()));
        onView(withId(R.id.taches_a_accomplir)).check(matches(hasChildCount(2)));
        onView(withId(R.id.taches_a_accomplir)).perform(actionOnItemAtPosition(0, click()));
        onView(withId(R.id.bouton_faire_peur)).perform(click());
        onView(withId(R.id.code_confirmation_tache_faite)).perform(replaceText("7"));
        onView(withText("OK")).perform(click());
        onView(withId(R.id.niveau_de_peur)).check(matches(withText("Intensité de peur du monstre: 1000 / 2000")));
        onView(withId(R.id.taches_a_accomplir)).check(matches(hasChildCount(1)));
        onView(withId(R.id.taches_a_accomplir)).perform(actionOnItemAtPosition(0, click()));
        onView(withId(R.id.bouton_faire_peur)).perform(click());
        onView(withId(R.id.code_confirmation_tache_faite)).perform(replaceText("7"));
        onView(withText("OK")).perform(click());
        onView(withId(R.id.liste_dialogue)).inRoot(isDialog()).check(matches(isDisplayed()));
        onView(withText("OK")).perform(click());
        onView(withId(R.id.bouton_ajouter_tache)).check(matches(isDisplayed()));
    }
}